import 'package:uuid/uuid.dart';
import 'package:news/NewsAPIJSONKeys.dart';

class NewsArticle {
  String id;
  String sourceName;
  String author;
  String title;
  String description;
  String url;
  String urlToImage;
  String publishedAt;

  NewsArticle.initWithJsonData(Map<String, dynamic> articleJson) {
    id = Uuid().v1().toString();
    final sourceJson =
        articleJson[NewsAPIJSONKeys.source] ?? Map<String, String>();
    sourceName = sourceJson[NewsAPIJSONKeys.name] ?? "";

    author = articleJson[NewsAPIJSONKeys.author] ?? "";
    title = articleJson[NewsAPIJSONKeys.title] ?? "";
    description = articleJson[NewsAPIJSONKeys.description] ?? "";
    url = articleJson[NewsAPIJSONKeys.url] ?? "";
    urlToImage = articleJson[NewsAPIJSONKeys.urlToImage] ?? "";
    publishedAt = articleJson[NewsAPIJSONKeys.publishedAt] ?? "";
  }
}
