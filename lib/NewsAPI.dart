import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:news/NewsAPIJSONKeys.dart';
import 'package:news/NewsArticle.dart';

class NewsAPI {
  static final _privateKey = "3b26660da1d24e0f9a0f55632fc86b21";

  static Future<List<NewsArticle>> fetchNewsArticles() async {
    var newsArticles = List<NewsArticle>();
    final response = await http.get(
        'https://newsapi.org/v2/top-headlines?country=us&apiKey=$_privateKey');

    if (response.statusCode == 200) {
      final jsonData = json.decode(response.body);
      final articles = jsonData[NewsAPIJSONKeys.articles];
      for (Map<String, dynamic> articleJson in articles) {
        final newsArticle = NewsArticle.initWithJsonData(articleJson);
        newsArticles.add(newsArticle);
      }
    } else {
      throw Exception('Failed to load news API');
    }

    return newsArticles;
  }
}
