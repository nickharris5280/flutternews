class NewsAPIJSONKeys {
  static final articles = "articles";
  static final source = "source";
  static final name = "name";
  static final author = "author";
  static final title = "title";
  static final description = "description";
  static final url = "url";
  static final urlToImage = "urlToImage";
  static final publishedAt = "publishedAt";
}
