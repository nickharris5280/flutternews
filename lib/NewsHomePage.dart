import 'dart:async';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:news/NewsAPI.dart';
import 'package:news/NewsArticle.dart';
import 'package:news/NewsArticleWidget.dart';

class NewsHomePage extends StatefulWidget {
  NewsHomePage({Key key}) : super(key: key);
  @override
  _NewsHomePageState createState() => _NewsHomePageState();
}

class _NewsHomePageState extends State<NewsHomePage> {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      GlobalKey<RefreshIndicatorState>();

  var _newsArticles = List<NewsArticle>();
  var _isFetchingNews = true;
  var _hasError = false;

  @override
  initState() {
    super.initState();
    _fetchNewsArticles();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("News"),
        ),
        body: SafeArea(
            child: Scrollbar(
                child: RefreshIndicator(
                    key: _refreshIndicatorKey,
                    onRefresh: _fetchNewsArticles,
                    child: _body))));
  }

  Widget get _body {
    if (_isFetchingNews) {
      return Center(child: CircularProgressIndicator());
    } else if (_hasError) {
      return Center(
        child: Text("Could not load news at this time"),
      );
    } else {
      return ListView.builder(
          padding: EdgeInsets.all(4.0),
          itemCount: _newsArticles.length,
          itemBuilder: (context, index) {
            final newsArticle = _newsArticles[index];
            return NewsArticleWidget(
                key: Key(newsArticle.id),
                sourceName: newsArticle.sourceName,
                author: newsArticle.author,
                title: newsArticle.title,
                description: newsArticle.description,
                url: newsArticle.url,
                urlToImage: newsArticle.urlToImage,
                publishedAt: newsArticle.publishedAt,
                tapAction: didTapNewsArticle);
          });
    }
  }

  Future<Null> _fetchNewsArticles() {
    final Completer<Null> completer = new Completer<Null>();
    NewsAPI.fetchNewsArticles().then((newsArticles) {
      setState(() {
        _newsArticles = newsArticles;
        _isFetchingNews = false;
        _hasError = false;
      });
      completer.complete(null);
    }).catchError((error) {
      setState(() {
        _newsArticles = [];
        _isFetchingNews = false;
        _hasError = true;
      });
    });

    return completer.future.then((_) {});
  }

  void didTapNewsArticle(String url) async {
    if (await canLaunch(url)) {
      await launch(url);
    }
  }
}
