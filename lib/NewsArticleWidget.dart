import 'package:flutter/material.dart';

typedef NewsArticleWidgetTapAction(String url);

class NewsArticleWidget extends StatelessWidget {
  final String sourceName;
  final String author;
  final String title;
  final String description;
  final String url;
  final String urlToImage;
  final String publishedAt;
  final NewsArticleWidgetTapAction tapAction;

  NewsArticleWidget(
      {Key key,
      @required this.sourceName,
      @required this.author,
      @required this.title,
      @required this.description,
      @required this.url,
      @required this.urlToImage,
      @required this.publishedAt,
      @required this.tapAction})
      : super(key: key);

  Widget build(BuildContext context) {
    return GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () => tapAction(url),
        child: Card(
            child: Container(
                margin: EdgeInsets.all(8.0),
                width: double.infinity,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    _titleWidget,
                    _sourceWidget,
                    _authorWidget,
                    _dividerWidget,
                    _descriptionWidget,
                    _pictureWidget
                  ],
                ))));
  }

  Widget get _titleWidget {
    if (title == null || title.length == 0) {
      return Container();
    } else {
      return Padding(
          padding: EdgeInsets.only(bottom: 4.0),
          child: Text(
            title,
            softWrap: true,
            style: TextStyle(fontSize: 22.0, fontWeight: FontWeight.bold),
          ));
    }
  }

  Widget get _sourceWidget {
    if (sourceName == null || sourceName.length == 0) {
      return Container();
    } else {
      return Text(
        sourceName,
        style: TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500),
      );
    }
  }

  Widget get _authorWidget {
    if (author == null || author.length == 0) {
      return Container();
    } else {
      return Padding(
          padding: EdgeInsets.only(top: 6.0),
          child: Text(author,
              style: TextStyle(fontSize: 11.0, fontStyle: FontStyle.italic)));
    }
  }

  Widget get _dividerWidget {
    if ((description == null || description.length == 0) &&
        (urlToImage == null || urlToImage.length == 0)) {
      return Container();
    } else {
      return Padding(
          padding: EdgeInsets.only(top: 12.0, bottom: 12.0),
          child: Divider(height: 1.0, color: Colors.grey));
    }
  }

  Widget get _descriptionWidget {
    if (description == null || description.length == 0) {
      return Container();
    } else {
      return Padding(
          padding: EdgeInsets.only(bottom: 12.0),
          child: Text(
            description,
            style: TextStyle(fontSize: 16.0),
          ));
    }
  }

  Widget get _pictureWidget {
    if (urlToImage == null || urlToImage.length == 0) {
      return Container();
    } else {
      return Image.network(urlToImage);
    }
  }
}
